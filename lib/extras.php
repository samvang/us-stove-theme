<?php

namespace USSC\Theme\Extras;

/**
 * Add <body> classes
 */
function body_class( $classes ) {
	// Add page slug if it doesn't exist.
	if ( is_single() || is_page() && ! is_front_page() ) {
		if ( ! in_array( basename( get_permalink() ), $classes, true ) ) {
			$classes[] = basename( get_permalink() );
		}
	}

	return $classes;
}

add_filter( 'body_class', __NAMESPACE__ . '\\body_class' );

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
	return ' &hellip; <a href="' . get_permalink() . '">' . __( 'Continued', 'usstove' ) . '</a>';
}

add_filter( 'excerpt_more', __NAMESPACE__ . '\\excerpt_more' );

/**
 * Convert %%date%% to date( 'Y' )
 *
 * @param string $value String to parse.
 *
 * @return mixed string.
 */
function copyright_date( $value ) {
	return ( $value )
		? str_replace( '%%date%%', date( 'Y' ), $value )
		: '';

}

add_filter( 'acf/format_value/name=footer_copyright', __NAMESPACE__ . '\\copyright_date' );

/**
 * Redirect generic searches to product search.
 */
function redirect_product_search_to_product_archive() {
	global $wp_query;

	$s         = $wp_query->get( 's' );
	$post_type = $wp_query->get( 'post_type' );

	if ( ! empty( $s ) && 'any' === $post_type ) {
		$url = add_query_arg(
			[
				's'         => $s,
				'post_type' => 'product',
			],
			home_url()
		);

		wp_safe_redirect( $url );
	}
}

add_action( 'template_redirect', __NAMESPACE__ . '\\redirect_product_search_to_product_archive' );

/**
 * Add title tags to images
 *
 * @param array        $attr       Attributes for the image markup.
 * @param \WP_Post     $attachment Image attachment post.
 * @param string|array $size       Requested size. Image size or array of width and height values
 *                                 (in that order). Default 'thumbnail'.
 *
 * @return mixed
 */
function add_title_tag_to_images( $attr, $attachment, $size ) {
	$attr['title'] = $attachment->post_title;

	return $attr;
}

add_filter( 'wp_get_attachment_image_attributes', __NAMESPACE__ . '\\add_title_tag_to_images', 10, 3 );
