<?php
/**
 * Filename edgenet.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme;

use Edgenet\Item\Product;
use USSC\Theme\Taxonomies\Fits_Stove_Type;

/**
 * Assign Edgenet Fits Stove Type Term to Post
 *
 * @param Product $product product being imported.
 * @param int     $post_id The Product's \WP_Post id.
 *
 * @return array|int|\WP_Error|bool
 */
function update_edgenet_fits_stove_type( $product, $post_id ) {
	// Get Fits Stove Type names.

	edgenet()->debug->notice( __( 'Hook: Fits Stove Type.', 'edgenet' ) );
	edgenet()->debug->indent();

	$fits_stove_type = $product->get_attribute_value( edgenet()->settings->get_field_map( '_fits_stove_type' ) );

	$status = [];

	if ( ! is_wp_error( $fits_stove_type ) && ! empty( $fits_stove_type ) ) {

		// add the term to the post.
		foreach ( explode( ',', $fits_stove_type ) as $term ) {

			edgenet()->debug->notice( __( sprintf( 'Setting Fits Stove Type term: %s', trim( $term ) ), 'edgenet' ) );

			$term_status = wp_set_object_terms( $post_id, trim( $term ), Fits_Stove_Type::TAXONOMY );

			if ( is_wp_error( $term_status ) ) {
				edgenet()->debug->error( __( 'Error setting Fits Stove Type term.', 'edgenet' ), $term_status );
			}

			$status[] = $term_status;
		}

	}

	edgenet()->debug->outdent();

	return $status;
}

add_action( 'edgenet_import_product_after_update', __NAMESPACE__ . '\\update_edgenet_fits_stove_type', 10, 2 );