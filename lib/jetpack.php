<?php
/**
 * Filename jetpack.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

/**
 * Setup Jetpack Infinite Scroll for WooCommerce products
 */
function setup_infinite_scroll() {
	add_theme_support( 'infinite-scroll', array(
		'type'           => 'click',
		'wrapper'        => 'l-card-grid',
		'footer_widgets' => false,
		'container'      => 'Products',
		'render'         => __NAMESPACE__ . '\\infinite_scroll_render',
	) );
}

add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup_infinite_scroll' );

/**
 * Render Jetpack Infinite Scroll.
 */
function infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();

		wc_get_template_part( 'content', 'product' );
	}
}

/**
 * Customize Infinite Scroll button text.
 *
 * @param $settings
 *
 * @return mixed
 */
function filter_jetpack_infinite_scroll_js_settings( $settings ) {
	$settings['text'] = __( 'Load More', 'ussc' );

	return $settings;
}

add_filter( 'infinite_scroll_js_settings', __NAMESPACE__ . '\\filter_jetpack_infinite_scroll_js_settings' );

/**
 * Dequeue Jetpack styles. We have our own.
 */
function dequeue_infinite_scroll_stylesheet() {

	wp_dequeue_style( 'the-neverending-homepage' );

}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\dequeue_infinite_scroll_stylesheet', 100 );

/**
 * Disable Lazy Loading for certain classes.
 *
 * @param $classes
 *
 * @return array
 */
function jetpack_lazy_images_blacklisted_classes( $classes ) {
	$classes[] = 'not-lazy';

	return $classes;
}

add_filter( 'jetpack_lazy_images_blacklisted_classes', __NAMESPACE__ . '\\jetpack_lazy_images_blacklisted_classes', 999, 1 );

/**
 * Remove default location for share links.
 */
function jetpack_likes_remove_share() {
	remove_filter( 'the_content', 'sharing_display', 19 );
	remove_filter( 'the_excerpt', 'sharing_display', 19 );
	if ( class_exists( 'Jetpack_Likes' ) ) {
		remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
	}
}

add_action( 'loop_start', 'jetpack_likes_remove_share' );

/**
 * Disable Jetpack's Open Graph tags when SEO Framework is installed.
 */
function disable_jetpack_open_graph() {
	if ( function_exists( 'the_seo_framework' ) ) {
		return false;
	}

	return true;

}

add_filter( 'jetpack_enable_open_graph', __NAMESPACE__ . '\\disable_jetpack_open_graph' );
