<?php
/**
 * Filename posts.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Posts;

function get_featured_posts() {
	$page_for_posts = get_option( 'page_for_posts' );
	if ( ! $page_for_posts ) {
		return [];
	}

	$featured_posts = get_field( 'featured_posts', $page_for_posts );

	if ( ! $featured_posts ) {
		// Get the latest post.
		$featured_posts = get_posts( [
			'posts_per_page'         => 1,
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );
	} else {
		// Flatten the array, Gets rid of the repeater dimension.
		$featured_posts = array_map( function ( $featured_post ) {
			return $featured_post['featured_post'];
		}, $featured_posts );
	}

	return ( is_array( $featured_posts ) )
		? $featured_posts
		: [];
}

function filter_featured_posts( $query ) {
	if ( ! is_home() ) {
		return;
	}

	remove_action( 'pre_get_posts', __NAMESPACE__ . '\\filter_featured_posts' );
	$featured_posts = get_featured_posts();
	add_action( 'pre_get_posts', __NAMESPACE__ . '\\filter_featured_posts' );

	$ppp = get_option( 'posts_per_page' );

	$query->set( 'posts_post_page', get_option( 'posts_per_page' ) + count( $featured_posts ) );

	if ( $query->is_paged ) {
		$page_offset = ( $query->query_vars['paged'] - 1 ) * $ppp;
		$query->set( 'offset', $page_offset );
	}

	return $query;
}

add_action( 'pre_get_posts', __NAMESPACE__ . '\\filter_featured_posts' );

function adjust_offset_pagination( $found_posts, $query ) {

	$featured_posts = get_featured_posts();

	if ( $query->is_home() ) {
		return $found_posts - count( $featured_posts );
	}

	return $found_posts;
}

add_filter( 'found_posts', __NAMESPACE__ . '\\adjust_offset_pagination', 1, 2 );

