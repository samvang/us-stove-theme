<?php
/**
 * Filename titles.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Titles;

/**
 * Page titles
 */
function title() {
	if ( is_home() ) {
		if ( get_option( 'page_for_posts', true ) ) {
			return get_the_title( get_option( 'page_for_posts', true ) );
		} else {
			return __( 'Latest Posts', 'usstove' );
		}
	} elseif ( is_archive() ) {
		return get_the_archive_title();
	} elseif ( is_search() ) {
		return sprintf( __( 'Search Results for &ldquo;%s&rdquo;', 'usstove' ), get_search_query() ); // phpcs:ignore
	} elseif ( is_404() ) {
		return __( 'Not Found', 'usstove' );
	} else {
		return get_the_title();
	}
}
