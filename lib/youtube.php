<?php
/**
 * Filename youtube.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\YouTube;

use WP_Perf\YouTube_Channel_Sync\Post_Types\Video;

add_filter( 'wpp_yt_video_html', __NAMESPACE__ . '\\wpp_yt_video_html', 10, 2 );

function wpp_yt_video_html( $html, $atts ) {

	$template = <<<HTML
<div class="column">
	<div class="c-card--video">
			<div class="c-card__thumb">
				<div class="responsive-embed widescreen">%s</div>
			</div>
			<div class="c-card__content">
				<h3 class="content__title">
					%s
				</h3>
			</div>
		</a>
	</div>
</div>
HTML;

	$html = sprintf( $template,
		get_post_meta( $atts['id'], Video::META_VIDEO_OEMBED, true ),
		get_the_title( $atts['id'] )
	);

	return $html;
}

add_filter( 'wpp_yt_playlist_videos_item_wrap_start', __NAMESPACE__ . '\\wpp_yt_playlist_videos_item_wrap_start', 10, 2);

function wpp_yt_playlist_videos_item_wrap_start($html, $atts) {
	return '';
}

add_filter( 'wpp_yt_playlist_videos_item_wrap_end', __NAMESPACE__ . '\\wpp_yt_playlist_videos_item_wrap_end', 10, 2);

function wpp_yt_playlist_videos_item_wrap_end($html, $atts) {
	return '';
}

add_filter( 'wpp_yt_playlist_videos_grid_wrap_start', __NAMESPACE__ . '\\wpp_yt_playlist_videos_grid_wrap_start', 10, 2 );

function wpp_yt_playlist_videos_grid_wrap_start( $html, $atts ) {
	return '<div class="row l-card-grid--3">';
}

add_filter( 'wpp_yt_playlist_videos_grid_wrap_end', __NAMESPACE__ . '\\wpp_yt_playlist_videos_grid_wrap_end', 10, 2 );

function wpp_yt_playlist_videos_grid_wrap_end( $html, $atts ) {
	return '</div>';
}
