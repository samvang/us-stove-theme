<?php
/**
 * Filename parts-finder.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Parts_Finder;

use function USSC\Theme\Helpers\is_parts_archive;

/**
 * Modify where clause to do keyword search across posts table.
 *
 * @param string    $where    Where clause.
 * @param \WP_Query $wp_query The query.
 *
 * @return string
 */
function posts_clauses( $clauses, $wp_query ) {
	if ( ! $wp_query->is_main_query() ) {
		return $clauses;
	}

	if ( ! is_parts_archive() ) {
		return $clauses;
	}

	$keywords = filter_input( INPUT_GET, 'keywords', FILTER_SANITIZE_STRING );

	if ( ! $keywords ) {
		return $clauses;
	}

	global $wpdb;
	$keywords = explode( ' ', $keywords );

	$postmeta_works_with = 'pm_' . uniqid();
	$postmeta_model_num  = 'pm_' . uniqid();

	$search_join = '';

	$search_join .= " LEFT JOIN {$wpdb->postmeta} AS $postmeta_works_with ON {$postmeta_works_with}.post_id = {$wpdb->posts}.ID";
	$search_join .= " LEFT JOIN {$wpdb->postmeta} AS $postmeta_model_num ON {$postmeta_model_num}.post_id = {$wpdb->posts}.ID";

	$clauses['join'] .= $search_join;

	$search_where = '';

	$search_where .= " AND {$postmeta_works_with}.meta_key = '_models_used_with'";
	$search_where .= " AND {$postmeta_model_num}.meta_key = '_model_no'";

	foreach ( $keywords as $keyword ) {
		$like = '%' . $wpdb->esc_like( trim( $keyword ) ) . '%';

		$search_where .= $wpdb->prepare(
			" AND ({$wpdb->posts}.post_title LIKE %s OR {$wpdb->posts}.post_content LIKE %s OR {$wpdb->posts}.post_excerpt LIKE %s OR {$postmeta_works_with}.meta_value LIKE %s OR {$postmeta_model_num}.meta_value LIKE %s)",
			$like, $like, $like, $like, $like
		);
	}

	$clauses['where'] .= $search_where;

	return $clauses;
}

add_filter( 'posts_clauses', __NAMESPACE__ . '\\posts_clauses', 10, 2 );
