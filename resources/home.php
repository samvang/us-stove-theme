<?php
/**
 * Filename home.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme\Posts;

$featured_posts     = Posts\get_featured_posts();
$featured_posts_ids = array_map( function ( $featured_post ) {
	return $featured_post->ID;
}, $featured_posts );

$ppp = get_option( 'posts_per_page' );

$count = 0;

?>
	<section class="c-carousel--design">
		<div class="row--wide column">
			<ul class="c-carousel__container js-carousel-design">
				<?php foreach ( $featured_posts as $featured_post ) : ?>
					<li
							class="c-carousel__slide"
							style="background-image: url(<?php echo esc_attr( get_the_post_thumbnail_url( $featured_post, 'full' ) ); ?>)"
					>
						<div class="row">
							<div class="column small-12 medium-5">
								<h2 class="title"><?php echo esc_html( $featured_post->post_title ); ?></h2>
								<div class="meta">
									<?php
									printf( '<span class="date">%s</span> by <span class="author">%s</span>',
										get_the_date( get_option( 'date_format' ), $featured_post->ID ),
										wp_kses_post( get_the_author_meta( 'display_name', $featured_post->post_author ) )
									);
									?>
								</div>
								<a href="<?php echo esc_attr( get_the_permalink( $featured_post->ID ) ); ?>" class="button">
									<?php esc_html_e( 'Learn More', 'usstove' ); ?>
								</a>
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</section>

	<div class="l-card-grid">
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php
			// Skip post if we've maxed the count.
			if ( $count ++ > $ppp ) {
				continue;
			}

			// Skip post if featured.
			if ( in_array( get_the_ID(), $featured_posts_ids, true ) ) {
				continue;
			}
			?>
			<div class="column">
				<?php get_template_part( 'partials/content', 'post' ); ?>
			</div>
		<?php endwhile; ?>
	</div>

<?php the_posts_navigation();
