<?php
/**
 * Filename functions.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

define( 'USSTOVE_ENV_PROD', 'prod' );
define( 'USSTOVE_ENV_STAGE', 'stage' );

if ( ! defined( 'USSTOVE_ENV' ) ) {
	$usstove_env = USSTOVE_ENV_PROD;
	if ( isset( $_SERVER['SERVER_NAME'] ) ) {
		switch ( $_SERVER['SERVER_NAME'] ) {
			case 'beta.usstove.com':
			case 'usstove.toibox.net':
			case 'usstove.test':
				$usstove_env = USSTOVE_ENV_STAGE;
				break;
			default:
				$usstove_env = USSTOVE_ENV_PROD;
				break;

		}
	}
	define( 'USSTOVE_ENV', $usstove_env );
}

/**
 * Helper function for prettying up errors
 *
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ( $message, $subtitle = '', $title = '' ) {
	$title   = $title ?: __( 'Sage &rsaquo; Error', 'usstove' );
	$footer  = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
	$message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
	wp_die( $message, $title );
};

/**
 * Ensure compatible version of PHP is used
 */
if ( version_compare( '7.0', phpversion(), '>=' ) ) {
	$sage_error( __( 'You must be using PHP 7.0 or greater.', 'usstove' ), __( 'Invalid PHP version', 'usstove' ) );
}

/**
 * Ensure compatible version of WordPress is used
 */
if ( version_compare( '4.7.0', get_bloginfo( 'version' ), '>=' ) ) {
	$sage_error( __( 'You must be using WordPress 4.7.0 or greater.', 'usstove' ), __( 'Invalid WordPress version', 'usstove' ) );
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map( function ( $file ) use ( $sage_error ) {
	$file = "../lib/{$file}.php";
	if ( ! locate_template( $file, true, true ) ) {
		$sage_error( sprintf( __( 'Error locating <code>%s</code> for inclusion.', 'usstove' ), $file ), 'File not found' );
	}
}, [
	// Theme resources.
	'assets',
	'setup',
	'wrapper',
	'titles',
	'extras',
	'options',
	'analytics',
	'faqs',
	'helpers',
	'posts',
	'parts-finder',
	'acf-hooks',
//	'advanced-custom-fields',
	// Nav helpers.
	'class-mega-nav-walker',
	'class-drilldown-nav-walker',
	'navs',
	// Plugin customizations.
	'woocommerce',
	'jetpack',
	'contact-form-7',
	// Edgenet customizations.
	'class-fits-stove-type',
	'edgenet',
	'youtube',
] );
