<?php
/**
 * Filename archive.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>

	<div class="l-intro">
		<div class="intro__head">
			<?php get_template_part( 'partials/page', 'header' ); ?>
		</div>
		<?php the_archive_description(); ?>
		<?php if ( ! empty( term_description() ) ) : ?>
			<div class="intro__copy">
				<?php echo term_description(); ?>
			</div>
		<?php endif; ?>
	</div>
	<div class="l-card-grid" id="Items">
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<div class="column">
				<?php get_template_part( 'partials/content', get_post_type() ); ?>
			</div>
		<?php endwhile; ?>
	</div>

<section class="c-pagination">
	<div class="row">
		<div class="column">
			<?php the_posts_pagination( [
				'mid_size'  => 3,
				'prev_next' => false,
			] ); ?>
		</div>
	</div>
</section>

