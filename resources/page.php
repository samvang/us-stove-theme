<?php
/**
 * Filename page.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>

<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
	<div class="column large-8 large-offset-2">
		<?php get_template_part( 'partials/page', 'header' ); ?>
		<div class="page-content">
			<?php get_template_part( 'partials/content', 'page' ); ?>
		</div>
	</div>
<?php endwhile;
