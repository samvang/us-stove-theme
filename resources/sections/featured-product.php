<?php
/**
 * Filename featured-product.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$featured_product = get_field( 'featured_product' );
?>
<section class="c-feat-product">
	<div class="row">
		<div class="column medium-offset-6 medium-6">
			<?php if ( isset( $featured_product['title'] ) ) : ?>
				<div class="title"><?php echo esc_html( $featured_product['title'] ); ?></div>
			<?php endif; ?>
			<?php if ( isset( $featured_product['description'] ) ) : ?>
				<p class="content show-for-medium"><?php echo esc_html( $featured_product['description'] ); ?></p>
			<?php endif; ?>
			<?php if ( isset( $featured_product['button_label'] ) && isset( $featured_product['button_link'] ) ) : ?>
				<p class="link">
					<a href="<?php echo esc_attr( $featured_product['button_link'] ); ?>" class="button hollow">
						<?php echo esc_html( $featured_product['button_label'] ); ?>
					</a>
				</p>
			<?php endif; ?>
		</div>
	</div>
	<?php if ( isset( $featured_product['background_image'] ) ) : ?>
		<style>
			.c-feat-product {
				background-image: url(<?php echo esc_attr( wp_get_attachment_image_url( $featured_product['background_image']['ID'], 'full' ) ); ?>);
			}
		</style>
	<?php endif; ?>
</section>
