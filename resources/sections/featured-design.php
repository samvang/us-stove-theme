<?php
/**
 * Filename featured-design.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$di = get_field( 'featured_design_inspiration' );

$latest = get_posts( [
	'numberposts' => 3,
] );

$curated = [];

if ( $di['feature_1'] instanceof WP_Post ) {
	$curated[] = $di['feature_1'];
}
if ( $di['feature_2'] instanceof WP_Post ) {
	$curated[] = $di['feature_2'];
}
if ( $di['feature_3'] instanceof WP_Post ) {
	$curated[] = $di['feature_3'];
}

$latest = array_udiff( $latest, $curated, function ( $latest, $curated ) {
	return ( $latest->ID - $curated->ID );
} );

if ( $di['feature_1'] instanceof WP_Post ) {
	$feat_1 = $di['feature_1'];
} else {
	if ( count( $latest ) ) {
		$feat_1 = array_shift( $latest );
	} else {
		$feat_1 = false;
	}
}

if ( $di['feature_2'] instanceof WP_Post ) {
	$feat_2 = $di['feature_2'];
} else {
	if ( count( $latest ) ) {
		$feat_2 = array_shift( $latest );
	} else {
		$feat_2 = false;
	}
}

if ( $di['feature_3'] instanceof WP_Post ) {
	$feat_3 = $di['feature_3'];
} else {
	if ( count( $latest ) ) {
		$feat_3 = array_shift( $latest );
	} else {
		$feat_3 = false;
	}
}

?>
<section class="c-feat-design">
	<div class="row">
		<div class="c-feat-design__intro column small-12">
			<?php if ( $di['title'] ) : ?>
				<h2 class="intro__title"><?php echo wp_kses_post( $di['title'] ); ?></h2>
			<?php endif; ?>
			<?php if ( $di['content'] ) : ?>
				<div class="intro__content">
					<?php echo wp_kses_post( $di['content'] ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="c-feat-design__cards js-carousel-feat-design">
		<?php if ( $feat_1 instanceof WP_Post ) : ?>
			<div class="cards__card-wrap">
				<div class="c-card--design">
					<a href="<?php echo esc_attr( get_permalink( $feat_1->ID ) ); ?>">
						<div class="c-card__thumb">
							<?php echo get_the_post_thumbnail( $feat_1->ID, 'card-thumb-design' ); ?>
						</div>
						<div class="c-card__content">
							<div class="content__meta">
								<?php
								printf( '<span class="date">%s</span> by <span class="author">%s</span>',
									get_the_date( get_option( 'date_format' ), $feat_1->ID ),
									esc_html( get_the_author_meta( 'display_name', $feat_1->post_author ) )
								);
								?>
							</div>
							<h3 class="content__title">
								<?php echo wp_kses_post( $feat_1->post_title ); ?>
							</h3>
						</div>
					</a>
				</div>
			</div>
		<?php endif; ?>
		<?php if ( $feat_2 instanceof WP_Post ) : ?>
			<div class="cards__card-wrap">
				<div class="c-card--design">
					<a href="<?php echo esc_attr( get_permalink( $feat_2->ID ) ); ?>">
						<div class="c-card__thumb">
							<?php echo get_the_post_thumbnail( $feat_2->ID, 'card-thumb-design' ); ?>
						</div>
						<div class="c-card__content">
							<div class="content__meta">
								<?php
								printf( '<span class="date">%s</span> by <span class="author">%s</span>',
									get_the_date( get_option( 'date_format' ), $feat_2->ID ),
									wp_kses_post( get_the_author_meta( 'display_name', $feat_2->post_author ) )
								);
								?>
							</div>
							<h3 class="content__title">
								<?php echo wp_kses_post( $feat_2->post_title ); ?>
							</h3>
						</div>
					</a>
				</div>
			</div>
		<?php endif; ?>
		<?php if ( $feat_3 instanceof WP_Post ) : ?>
			<div class="cards__card-wrap">
				<div class="c-card--design">
					<a href="<?php echo esc_attr( get_permalink( $feat_3->ID ) ); ?>">
						<div class="c-card__thumb">
							<?php echo get_the_post_thumbnail( $feat_3->ID, 'card-thumb-design' ); ?>
						</div>
						<div class="c-card__content">
							<div class="content__meta">
								<?php
								printf( '<span class="date">%s</span> by <span class="author">%s</span>',
									get_the_date( get_option( 'date_format' ), $feat_3->ID ),
									wp_kses_post( get_the_author_meta( 'display_name', $feat_3->post_author ) )
								);
								?>
							</div>
							<h3 class="content__title">
								<?php echo wp_kses_post( $feat_3->post_title ); ?>
							</h3>
						</div>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
