<?php
/**
 * Template Name: Video Gallery Template
 */

$playlists = get_field( 'playlists' );
?>

<div class="row column">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'partials/page', 'header' ); ?>
		<div class="page-content">
			<?php get_template_part( 'partials/content', 'page' ); ?>
		</div>

		<?php if ( $playlists ) : ?>
			<section class="c-video-playlists">
				<ul class="accordion" data-accordion data-allow-all-closed="true">
					<?php foreach ( $playlists as $playlist ) : ?>
						<li class="accordion-item" data-accordion-item>
							<a href="#" class="accordion-title playlists__title"><?php echo $playlist['playlist_term']->name; ?></a>

							<div class="accordion-content" data-tab-content>
								<div class="playlists__playlist">
									<?php
									echo \WP_Perf\YouTube_Channel_Sync\Taxonomies\Playlist::render_shortcode(
										[
											'id'      => $playlist['playlist_term']->term_id,
											'display' => 'grid',
											'cols'    => 3,
										]
									);
									?>
								</div>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</section>
		<?php endif; ?>
	<?php endwhile; ?>
</div>
