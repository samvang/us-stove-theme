<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( 'column' ); ?>>
	<div class="c-card--prod">
		<a href="<?php the_permalink(); ?>">
			<div class="c-card__thumb">
				<?php echo woocommerce_get_product_thumbnail(); /* phpcs:ignore */ ?>
				<div class="thumb__overlay">
					<button class="button hollow white"><?php esc_html_e( 'View Product', 'ussc' ); ?></button>
				</div>
			</div>

			<div class="c-card__content">
				<?php $model_no = get_field('ussc_model_no'); ?>
				<?php if ( $model_no ) : ?>
					<span class="content__model_num"><?php echo esc_html( $model_no ); ?></span>
				<?php endif; ?>
				<h2 class="content__title"><?php the_title(); ?></h2>
			</div>
		</a>
	</div>
</li>
