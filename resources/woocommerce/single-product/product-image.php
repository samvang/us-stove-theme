<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

/**
 * @var WC_Product $product Current product instance.
 */
global $product;

// Get images.
$post_thumbnail_id = $product->get_image_id();
$attachment_ids    = $product->get_gallery_image_ids();

// Ensure primary image is first.
array_unshift( $attachment_ids, (int) $post_thumbnail_id );
$attachment_ids = array_unique( $attachment_ids );

// When has_thumbs is set, we display the thumbnail strip.
$has_thumbs = ( ! empty( $attachment_ids ) && 1 < count( $attachment_ids ) );
?>
<div class="c-product-gallery">
	<?php if ( $has_thumbs ) : ?>
		<div class="c-product-gallery__thumbs show-for-large">
			<div class="thumbs__wrap">
				<div class="js-gallery-thumbs">
					<?php foreach ( $attachment_ids as $attachment_id ) : ?>
						<div class="thumbs__image">
							<?php echo wp_get_attachment_image( $attachment_id, 'woocommerce_gallery_thumbnail', false, [ 'class' => 'not-lazy' ] ); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if ( ! empty( $attachment_ids ) ) : ?>
		<div class="c-product-gallery__fullsize <?php echo ( $has_thumbs ) ? 'has-thumbs' : ''; ?>">
			<div class="fullsize__wrap">
				<div class="js-gallery-fullsize">
					<?php foreach ( $attachment_ids as $attachment_id ) : ?>
						<div class="fullsize__image">
							<?php echo wp_get_attachment_image( $attachment_id, 'woocommerce_single', false, [ 'class' => 'not-lazy' ] ); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
