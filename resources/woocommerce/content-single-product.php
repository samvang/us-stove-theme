<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

?>
<div class="row column">
	<?php
	/**
	 * Hook: woocommerce_before_single_product.
	 *
	 * @hooked wc_print_notices - 10
	 */
	do_action( 'woocommerce_before_single_product' );
	?>
</div>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'l-product c-product' ); ?>>
	<div class="row">
		<div class="column small-12 hide-for-large">
			<h1 class="product__title"><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="row product__overview">
		<div class="column small-12 large-6">
			<?php wc_get_template( 'single-product/product-image.php' ); ?>
		</div>

		<div class="product__summary column small-12 large-6">
			<h1 class="product__title show-for-large"><?php the_title(); ?></h1>
			<div class="product__description">
				<?php the_content(); ?>
				<ul class="description__specs">
					<?php
					$model_no = get_field('ussc_model_no', get_the_ID());
//					$model_no = get_post_meta(get_the_ID(), '_sku', true);
					if ( $model_no ) {
						printf( '<li class="specs__spec"><span class="spec__label">Model #:</span> %s</li>', esc_html( $model_no ) );
					}
					?>
				</ul>
			</div>
			<div class="product__actions">
				<div class="actions__price">
					<?php woocommerce_template_single_price(); ?>
				</div>
				<div class="actions__ctas">
					<?php
					$reseller        = false;
					$product_cat_ids = wc_get_product_term_ids( get_the_ID(), 'product_cat' );
					foreach ( $product_cat_ids as $product_cat_id ) {
						if ( get_term_meta( $product_cat_id, 'ussc_reseller_only', true ) ) {
							$reseller = true;
							break;
						}
					}
					?>
					<?php if ( $reseller ) : ?>
						<button type="button" class="button" data-open="where-to-buy"><?php esc_html_e( 'Where to buy', 'usstove' ); ?></button>
					<?php else : ?>
						<?php woocommerce_template_single_add_to_cart(); ?>
					<?php endif; ?>
				</div>
			</div>

			<?php do_action( 'woocommerce_single_product_summary' ); ?>
		</div>
	</div>
	<div class="row">
		<div class="column small-12">
			<?php wc_get_template( 'single-product/tabs/tabs.php' ); ?>
		</div>
	</div>

	<div class="row">
		<div class="column small-12">
			<?php
			$args = array(
				'posts_per_page' => 4,
				'columns'        => 4,
				'orderby'        => 'rand', // @codingStandardsIgnoreLine.
			);

			woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
			?>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
