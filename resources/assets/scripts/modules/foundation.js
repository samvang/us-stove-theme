import $ from 'jquery';
import 'foundation-sites/dist/js/foundation';

export default {
	init() {
		$( document ).foundation();

		$( 'body' ).on( 'resizeme.zf.trigger', function() {
			const $drilldown = $( '.drilldown' );
			$drilldown.foundation( '_destroy' );
			$drilldown.foundation();
		} );
	},
};