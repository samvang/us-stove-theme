import $ from 'jquery';

const config = {
	selector: {
		form: '.js-parts-finder',
		filter: '.js-filter',
		clear: '.js-clear',
		overlay: '.js-overlay',
	},
};

const el = {};

export default {
	init() {

		el.$form = $( config.selector.form );

		if ( el.$form.length === 0 ) {
			return false;
		}

		el.$overlay = $( config.selector.overlay );

		el.$filters = el.$form.find( '.js-filter' );

		el.$clear = el.$form.find( '.js-clear' );

		el.$filters.on( 'change', function( e ) {
			el.$filters.not( e.target ).removeAttr( 'checked' );
			el.$overlay.show();
			el.$form.submit();
		} );

		el.$clear.on( 'click', function() {
			el.$filters.removeAttr( 'checked' );
			el.$overlay.show();
			el.$form.submit();
		} );
	},
};
