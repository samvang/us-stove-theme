import $ from 'jquery';

export default {
  init() {
    $( '.label--optional-text input' ).on( 'keyup', function() {
      const inputValue = $( this ).val();
      if ( inputValue === '' ) {
        $( this ).parent().addClass( 'is-input-empty' );
        console.log( 'EMPTY' );
      } else {
        $( this ).parent().removeClass( 'is-input-empty' );
      }
    } );

    // Show filename of uploaded file on file inputs.
    const $files = $( 'input[type=file]' );
    $files.each( function( i, el ) {
      const $el = $( el );
      $el.after( '<span class="filename"></span>' );
      $el.on( 'change', function() {
        $el.next().text( this.files[ 0 ].name );
      } );
    } );

    $( 'button[type="submit"]' ).click( function() {
      $( this ).find( '.js-spinner' ).css( 'display', 'inline-block' );
    } );

    $( '.wpcf7-submit' ).click( function() {
      $( this ).attr( 'disabled', true );
    } );

    document.addEventListener( 'wpcf7submit', function( event ) {
      $( '#' + event.detail.id + ' .wpcf7-submit' ).attr( 'disabled', false );
      $( '#' + event.detail.id ).find( '.js-spinner' ).css( 'display', 'none' );
    }, false );
  },

};
