import $ from 'jquery';

export default {
	init() {
		$( '.js-mega-menu' ).click( function( e ) {
			const menuId = e.target.getAttribute( 'data-mega-menu-id' );

			const targetPanel = document.getElementById( menuId );

			const allPanels = $( '.js-mega-menu-panel' );

			const otherPanels = $( allPanels ).not( targetPanel );

			$( otherPanels ).removeClass( 'is-active' );
			targetPanel.classList.toggle( 'is-active' );
			return false;
		} );

		$( 'body' ).click( function( e ) {
			const $activeMenu = $( '.js-mega-menu-panel.is-active' );

			if ( !$activeMenu.is( e.target ) && !$activeMenu.has( e.target ).length > 0 ) {
				console.log( $activeMenu );
				console.log( e.target );
				const allPanels = $( '.js-mega-menu-panel' );
				allPanels.removeClass( 'is-active' );
			}

		} );
	},

};
