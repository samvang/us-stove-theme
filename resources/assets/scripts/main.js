// External Dependencies.
import $ from 'jquery';

// Module_Loader
import ModuleLoader from './utils/ModuleLoader';

// Modules
import foundation from './modules/foundation';
import carousels from './modules/carousels';
import mega_menu from './modules/mega_menu';
import pencil_ad from './modules/pencil_ad';
import form from './modules/form';
import parts_filters from './modules/parts_filters';

const modules = new ModuleLoader( {
	foundation,
	carousels,
	mega_menu,
	pencil_ad,
	form,
	parts_filters,
} );

$( document ).ready( () => modules.init() );
