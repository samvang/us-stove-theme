<?php
/**
 * Filename base.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme\Wrapper;

?><!doctype html>
<html <?php language_attributes(); ?>>
<?php get_template_part( 'partials/head' ); ?>
<body <?php body_class(); ?> data-resize="body">
<?php
do_action( 'get_header' );
get_template_part( 'partials/pencil-ad' );
get_template_part( 'partials/title-bar' );
?>

<div class="off-canvas-wrapper">
	<div class="c-off-canvas--nav off-canvas position-right" id="OffCanvasNav" data-off-canvas data-transition="push">
		<?php get_template_part( 'partials/header-off-canvas' ); ?>
	</div><!-- /.c-off-canvas--nav -->

	<div class="c-off-canvas--search off-canvas position-top" id="OffCanvasSearch" data-off-canvas data-transition="push">
		<?php get_template_part( 'partials/search-off-canvas' ); ?>
	</div><!-- /.c-off-canvas--search -->

	<div class="off-canvas-content" data-off-canvas-content>

		<?php get_template_part( 'partials/header' ); ?>

		<div class="l-wrap" role="document">

			<?php if ( function_exists( 'bcn_display' ) && ! is_front_page() && ! is_404() && ! is_search() ) : ?>
				<div class="row--wide column show-for-large">
					<div class="c-breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
						<?php bcn_display_list(); ?>
					</div>
				</div>
			<?php endif; ?>

			<main class="l-main" role="main">
				<?php require Wrapper\template_path(); ?>
			</main><!-- /.l-main -->

		</div><!-- /.l-wrap -->

	</div><!-- /.off-canvas-content -->

	<?php get_template_part( 'partials/footer' ); ?>

</div><!-- /.off-canvas-wrapper -->

<?php get_template_part( 'partials/modal-where-to-buy' ); ?>

<?php get_template_part( 'partials/modal-search' ); ?>

<?php
do_action( 'get_footer' );
wp_footer();
?>

</body>
</html>
