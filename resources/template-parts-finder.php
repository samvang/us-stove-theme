<?php
/**
 * Filename template-parts-finder.php
 * Template Name: Parts Finder Template
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>

<?php get_template_part( 'partials/page', 'header' ); ?>
<?php get_template_part( 'partials/parts-filters' ); ?>

<?php
$args = [
	'post_type' => 'product',
];

$keywords = '';
if ( ! empty( $_GET['keywords'] ) ) { // phpcs:ignore
	$keywords  = filter_input( INPUT_GET, 'keywords', FILTER_SANITIZE_STRING );
	$args['s'] = $keywords;
}

$selected_terms = [];

if ( ! empty( $_GET['fits-stove-type'] ) ) { // phpcs:ignore
	$selected_terms = filter_input( INPUT_GET, 'fits-stove-type', FILTER_DEFAULT, [ 'flags' => FILTER_FORCE_ARRAY ] );
	$selected_terms = array_map( function ( $term ) {
		return sanitize_title( $term );
	}, $selected_terms );
}


$parts_root_tax = get_field( 'parts_finder_root_product_cat', 'option' );

$tax_query = [
	'relation' => 'AND',
	[
		'taxonomy'         => 'product_cat',
		'field'            => 'id',
		'terms'            => $parts_root_tax,
		'include_children' => true,
	],
];

if ( count( $selected_terms ) > 0 ) {
	$tax_query = [
		[
			'taxonomy' => \USSC\Theme\Taxonomies\Fits_Stove_Type::TAXONOMY,
			'field'    => 'slug',
			'terms'    => $selected_terms,
			'operator' => 'IN',
		],
	];
}

$args['tax_query'] = $tax_query; // phpcs:ignore

$parts_query = new WP_Query( $args );
?>

<div class="row">
	<div class="column small-12">
		<?php if ( ! have_posts() ) : ?>
			<div class="alert alert-warning">
				<?php esc_html_e( 'Sorry, no results were found.', 'usstove' ); ?>
			</div>
			<?php get_product_search_form(); ?>
		<?php endif; ?>
	</div>
</div>

<div class="row">
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( $parts_query->have_posts() ) {
		while ( $parts_query->have_posts() ) {
			$parts_query->the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );

	/**
	 * Hook: woocommerce_after_main_content.
	 *
	 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	 */
	do_action( 'woocommerce_after_main_content' );
	?>
</div>

<?php the_posts_navigation(); ?>
