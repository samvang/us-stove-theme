<?php
/**
 * Template Name: Custom Template
 */
?>

<div class="row column">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'partials/page', 'header' ); ?>
		<?php get_template_part( 'partials/content', 'page-custom' ); ?>
	<?php endwhile; ?>
</div>