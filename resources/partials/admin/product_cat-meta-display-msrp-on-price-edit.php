<?php
/**
 * Filename product_cat-meta-display-msrp-on-price.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$tag_id                = $_REQUEST['tag_ID']; // phpcs:ignore
$display_msrp_on_price = get_term_meta( $tag_id, 'ussc_display_msrp_on_price', true );
?>
<tr class="form-field form-required display-msrp-on-price-wrap">
	<th scope="row">
		<label for="tag-display-msrp-on-price">
			<?php esc_html_e( 'Display MSRP on Price', 'usstove' ); ?>
		</label>
	</th>
	<td>
		<label for="tag-display-msrp-on-price">
			<input name="ussc_display_msrp_on_price" id="tag-display-msrp-on-price" type="checkbox" value="on" <?php checked( $display_msrp_on_price ); ?>>
			<?php esc_html_e( 'Display MSRP on Price', 'usstove' ); ?>
		</label>
		<p class="description">
			<?php esc_html_e( 'Products in this category will have a small label shown under the price saying "Suggested MSRP". Note: this applies only to the selected category, you must repeat this action for any child categories.', 'usstove' ); ?>
		</p>
	</td>
</tr>
