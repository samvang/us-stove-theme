<?php
/**
 * Filename product_cat-meta-hide-price.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<div class="form-field term-hide-price-wrap">
	<label for="tag-hide-price">
		<input name="ussc_hide_price" id="tag-hide-price" type="checkbox" value="on">
		<?php esc_html_e( 'Hide Price', 'usstove' ); ?>
	</label>
	<p class="description">
		<?php esc_html_e( 'Products in this category will not display price information. Should be used in combination with Reseller Only. Note: this applies only to the selected category, you must repeat this action for any child categories.', 'usstove' ); ?>
	</p>
</div>
