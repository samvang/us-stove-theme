<?php
/**
 * Filename product_cat-meta-reseller-only.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$tag_id        = $_REQUEST['tag_ID']; // phpcs:ignore
$reseller_only = get_term_meta( $tag_id, 'ussc_reseller_only', true );
?>
<tr class="form-field form-required reseller-only-wrap">
	<th scope="row">
		<label for="tag-reseller-only">
			<?php esc_html_e( 'Reseller Only', 'usstove' ); ?>
		</label>
	</th>
	<td>
		<label for="tag-reseller-only">
			<input name="ussc_reseller_only" id="tag-reseller-only" type="checkbox" value="on" <?php checked( $reseller_only ); ?>>
			<?php esc_html_e( 'Reseller Only', 'ussc' ); ?>
		</label>
		<p class="description">
			<?php esc_html_e( 'Products in this category are only available for purchase via resellers and display a link for "Where to Buy". Note: this applies only to the selected category, you must repeat this action for any child categories that should also be marked as reseller only.', 'usstove' ); ?>
		</p>
	</td>
</tr>
