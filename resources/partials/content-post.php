<?php
/**
 * Filename content-post.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<div class="c-card--design">
	<a href="<?php echo esc_attr( get_permalink() ); ?>">
		<div class="c-card__thumb">
			<?php echo get_the_post_thumbnail( get_the_ID(), 'card-thumb-design' ); ?>
		</div>
		<div class="c-card__content">
			<div class="content__meta">
				<?php
				printf( '<span class="date">%s</span> by <span class="author">%s</span>',
					get_the_date( get_option( 'date_format' ) ),
					wp_kses_post( get_the_author_meta( 'display_name' ) )
				);
				?>
			</div>
			<h3 class="content__title">
				<?php the_title(); ?>
			</h3>
		</div>
	</a>
</div>
