<?php
/**
 * Filename parts-filters.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$keywords = '';

if ( ! empty( $_GET['keywords'] ) ) { // phpcs:ignore
	$keywords = filter_input( INPUT_GET, 'keywords', FILTER_SANITIZE_STRING );
}

$all_terms = get_terms( [ 'taxonomy' => \USSC\Theme\Taxonomies\Fits_Stove_Type::TAXONOMY ] );

$selected_terms = [];

if ( ! empty( $_GET['fits-stove-type'] ) ) { // phpcs:ignore
	$selected_terms = filter_input( INPUT_GET, 'fits-stove-type', FILTER_DEFAULT, [ 'flags' => FILTER_FORCE_ARRAY ] );
	$selected_terms = array_map( function ( $term ) {
		return sanitize_title( $term );
	}, $selected_terms );
}
?>

<form class="c-parts-finder js-parts-finder">
	<div class="row">
		<div class="column small-12 large-8">
			<!--
			<h2><?php esc_html_e( 'Filter by stove type:', 'usstove' ); ?></h2>
			<div class="c-parts-filters js-parts-filters">
				<fieldset>
					<?php foreach ( $all_terms as $term ) : ?>
						<label class="filter__label" for="fits-stove-type-<?php echo $term->term_id; ?>">
							<?php echo $term->name; ?>
							<input
									type="checkbox"
									id="fits-stove-type-<?php echo $term->term_id; ?>"
									name="fits-stove-type[]"
									value="<?php echo $term->slug; ?>"
									class="js-filter"
								<?php checked( in_array( $term->slug, $selected_terms, true ) ); ?>
							>
						</label>
					<?php endforeach; ?>
					<a href="#" class="button small filter__btn js-clear"><?php _e( 'Clear Filters', 'usstove' ); ?></a>

				</fieldset>
			</div>
			-->
		</div>
		<div class="column small-12 large-4">
			<h2><?php esc_html_e( 'Part search:', 'usstove' ); ?></h2>
			<div class="c-parts-search js-parts-search">
				<div class="input-group">
					<input
							class="input-group-field"
							type="text"
							name="keywords"
							value="<?php echo esc_attr( $keywords ); ?>"
							placeholder="<?php _e( 'Name, Model #, Stove Model #', 'usstove' ); ?>"
					>
					<div class="input-group-button">
						<button type="submit" class="button js-submit"><?php _e('Search', 'usstove'); ?><span class="js-spinner usstove-spinner"></span></button>
					</div>
				</div>
				<p><?php _e( 'Search by part name, part model #, or stove model #', 'usstove' ); ?></p>
			</div>
		</div>
	</div>
</form>
