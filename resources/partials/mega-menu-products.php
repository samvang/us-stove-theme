<?php
/**
 * Filename mega-menu-products.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$menu = get_field( 'mega_menu_products', 'option' );

?>
<div id="mega-menu-products" class="c-mega-menu__panel--products js-mega-menu-panel">
	<div class="row--wide">
		<?php if ( ! empty( $menu['sections'] ) ) : ?>
			<?php foreach ( $menu['sections'] as $section ) : ?>
				<?php $term = get_term( $section['primary_product_cat'], 'product_cat' ); ?>
				<?php if ( $term instanceof WP_Term ) : ?>
					<div class="medium-4 column">
						<div class="row--4 collapse panel__product">
							<div class="col-1">
								<?php
								printf( '<a href="%s">%s</a>',
									esc_attr( get_term_link( $term->term_id ) ),
									wp_get_attachment_image( $section['image'], 'mega-menu-thumb' )
								);
								?>
							</div>
							<div class="col-3">
								<h3 class="panel__title">
									<a href="<?php echo esc_attr( get_term_link( $term->term_id ) ); ?>">
										<?php echo esc_html( $term->name ); ?>
									</a>
								</h3>
								<p class="panel__description">
									<?php echo esc_html( $section['description'] ); ?>
								</p>
								<div class="panel__links">
									<?php if ( empty( $section['secondary_product_cats'] ) ) : ?>
										<a class="panel__link" href="<?php echo esc_attr( get_term_link( $term->term_id ) ); ?>">
											<?php esc_html_e( 'View All', 'ussc' ); ?>
										</a>
									<?php else : ?>
										<?php foreach ( $section['secondary_product_cats'] as $secondary_product_cat ) : ?>
											<?php
											$term = get_term( $secondary_product_cat['product_cat'] );
											if ( $term instanceof WP_Term ) {
												printf( '<a class="panel__link" href="%s">%s</a>',
													esc_attr( get_term_link( $term->term_id ) ),
													esc_html( $term->name )
												);
											}
											?>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
