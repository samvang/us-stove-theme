<?php
/**
 * Filename header-off-canvas.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme;

$logo = get_field( 'header_logo', 'option' );

$social_links = [];

$social_links['facebook']  = get_field( 'facebook_url', 'option' );
$social_links['twitter']   = get_field( 'twitter_url', 'option' );
$social_links['instagram'] = get_field( 'instagram_url', 'option' );
$social_links['youtube']   = get_field( 'youtube_url', 'option' );

$social_links = array_filter( $social_links );
?>
<header class="l-header--mobile hide-for-large">
	<nav class="c-nav-mobile">
		<?php if ( has_nav_menu( 'mobile_nav' ) ) : ?>
			<?php
			wp_nav_menu( [
				'theme_location' => 'mobile_nav',
				'items_wrap'     => '<ul id="%1$s" class="%2$s" data-responsive-menu="drilldown">%3$s</ul>',
				'menu_class'     => 'c-nav-mobile__nav vertical menu',
				'walker'         => new Theme\Drilldown_Nav_Walker(),
			] );
			?>
		<?php endif; ?>
		<div class="c-nav-mobile__footer">
			<div class="footer__brand">
				<a class="brand__link" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php echo wp_get_attachment_image( $logo['ID'], 'full', false, [ 'class' => 'brand__img' ] ); ?>
				</a>
			</div>
			<div class="footer__social">
				<div class="social__title">
					<span><?php esc_html_e( 'Follow Us', 'ussc' ); ?></span>
				</div>
				<?php if ( ! empty( $social_links ) ) : ?>
					<div class="social__links">
						<ul>
							<?php if ( isset( $social_links['facebook'] ) ) : ?>
								<li>
									<a href="<?php echo esc_attr( $social_links['facebook'] ); ?>">
										<i class="usstove-icon_social_Facebook"></i>
									</a>
								</li>
							<?php endif; ?>
							<?php if ( isset( $social_links['twitter'] ) ) : ?>
								<li>
									<a href="<?php echo esc_attr( $social_links['twitter'] ); ?>">
										<i class="usstove-icon_social_Twitter"></i>
									</a>
								</li>
							<?php endif; ?>
							<?php if ( isset( $social_links['instagram'] ) ) : ?>
								<li>
									<a href="<?php echo esc_attr( $social_links['instagram'] ); ?>">
										<i class="usstove-icon_social_Instagram"></i>
									</a>
								</li>
							<?php endif; ?>
							<?php if ( isset( $social_links['youtube'] ) ) : ?>
								<li>
									<a href="<?php echo esc_attr( $social_links['youtube'] ); ?>">
										<i class="usstove-icon_social_Youtube"></i>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</nav>
</header>
