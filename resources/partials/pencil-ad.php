<?php
/**
 * Filename pencil-ad.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$is_enabled = get_field( 'pencil_ad_is_enabled', 'option' );
$pencil_ad  = get_field( 'pencil_ad_content', 'option' );
?>
<?php if ( $is_enabled && $pencil_ad ) : ?>
	<?php
	$is_dismissible = ( get_field( 'pencil_ad_is_dismissable', 'options' ) )
		? 'is-dismissable'
		: '';

	$uid = hash( 'md5', $pencil_ad );
	?>
	<div class="c-pencil-ad js-pencil-ad <?php echo esc_attr( $is_dismissible ); ?>" data-uid="<?php echo esc_attr( $uid ); ?>">
		<div class="row--wide column">
			<div class="c-pencil-ad__ad">
				<?php
				printf( '%s',
					wp_kses(
						$pencil_ad,
						[
							'p' => true,
							'a' => [
								'href' => true,
							],
						]
					)
				);
				?>
			</div>
			<?php if ( $is_dismissible ) : ?>
				<button class="c-pencil-ad__close js-pencil-ad-close" type="button"></button>
			<?php endif; ?>
		</div>
	</div>
<?php endif;
