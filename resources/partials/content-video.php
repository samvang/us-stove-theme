<?php
/**
 * Filename content-post.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<div class="c-card--design">
	<a href="<?php echo esc_attr( get_permalink() ); ?>">
		<div class="c-card__thumb">
			<?php echo get_the_post_thumbnail( get_the_ID(), 'card-thumb-design' ); ?>
		</div>
		<div class="c-card__content">
			<h3 class="content__title">
				<?php the_title(); ?>
			</h3>
		</div>
	</a>
</div>
