<?php
/**
 * Filename content-single.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
	<div class="row">
		<div class="column small-12 medium-offset-1 medium-10">
			<article <?php post_class(); ?>>
				<header>
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php // get_template_part( 'partials/entry-meta' ); ?>
				</header>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				<footer>
					<?php
					wp_link_pages(
						[
							'before' => '<nav class="page-nav"><p>' . __( 'Pages:', 'usstove' ),
							'after'  => '</p></nav>',
						]
					);
					?>
				</footer>
			</article>
		</div>
		<aside class="column small-12 medium-4">
			<?php
			if ( function_exists( 'sharing_display' ) ) {
				sharing_display( '', true );
			}

			if ( class_exists( 'Jetpack_Likes' ) ) {
				$custom_likes = new Jetpack_Likes;
				echo $custom_likes->post_likes( '' );
			}
			?>
		</aside>
	</div>
<?php endwhile;

