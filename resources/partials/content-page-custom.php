<form class="ussc-form">
  <h2 class="ussc-form__title">Register for USStove.com</h2>
  <p class="ussc-form__note">All fields are required unless otherwise indicated.</p>
  <fieldset class="ussc-form__fieldset">
    <legend>Your Information:</legend>
    <div class="row">
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">First name</span>
          <input type="text" placeholder="Enter your first name">
        </label>
      </div>
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Last name</span>
          <input type="text" placeholder="Enter your last name">
        </label>
      </div>
    </div>
    <div class="row">
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Address</span>
          <input type="text" placeholder="Enter your address">
        </label>
      </div>
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">City</span>
          <input type="text" placeholder="Enter your city">
        </label>
      </div>
    </div>
    <div class="row">
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">State</span>
          <select>
            <option disabled value="null">Select your state</option>
            <option value="alabama">Alabama</option>
            <option value="alaska">Alaska</option>
            <option value="arizona">Arizona</option>
            <option value="arkansas">Arkansas</option>
            <option value="california">California</option>
            <option value="colorado">Colorado</option>
            <option value="connecticut">Connecticut</option>
            <option value="delaware">Delaware</option>
            <option value="florida">Florida</option>
            <option value="georgia">Georgia</option>
            <option value="hawaii">Hawaii</option>
            <option value="idaho">Idaho</option>
            <option value="illinois">Illinois</option>
            <option value="indiana">Indiana</option>
            <option value="iowa">Iowa</option>
            <option value="kansas">Kansas</option>
            <option value="kentucky">Kentucky</option>
            <option value="louisiana">Louisiana</option>
            <option value="maine">Maine</option>
            <option value="maryland">Maryland</option>
            <option value="massachusetts">Massachusetts</option>
            <option value="michigan">Michigan</option>
            <option value="minnesota">Minnesota</option>
            <option value="mississippi">Mississippi</option>
            <option value="missouri">Missouri</option>
            <option value="montana">Montana</option>
            <option value="nebraska">Nebraska</option>
            <option value="nevada">Nevada</option>
            <option value="new Hampshire">New</option>
            <option value="new Jersey">New</option>
            <option value="new Mexico">New</option>
            <option value="new York">New</option>
            <option value="north Carolina">North</option>
            <option value="north Dakota">North</option>
            <option value="ohio">Ohio</option>
            <option value="oklahoma">Oklahoma</option>
            <option value="oregon">Oregon</option>
            <option value="pennsylvania">Pennsylvania</option>
            <option value="rhode Island">Rhode</option>
            <option value="south Carolina">South</option>
            <option value="south Dakota">South</option>
            <option value="tennessee">Tennessee</option>
            <option value="texas">Texas</option>
            <option value="utah">Utah</option>
            <option value="vermont">Vermont</option>
            <option value="virginia">Virginia</option>
            <option value="washington">Washington</option>
            <option value="west Virginia">West</option>
            <option value="wisconsin">Wisconsin</option>
            <option value="wyoming">Wyoming</option>
          </select>
        </label>
      </div>
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Country</span>
          <select>
            <option disabled value="null">Select your country</option>
            <option value="usa">USA</option>
            <option value="canada">Canada</option>
            <option value="mexico">Mexico</option>
            <option value="france">France</option>
            <option value="uk">UK</option>
          </select>
        </label>
      </div>
    </div>
    <div class="row">
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Zip Code</span>
          <input type="text" placeholder="Enter your Zip Code">
        </label>
      </div>
    </div>
    <div class="row">
      <div class="column small-12">
        <input id="shipping-address" type="checkbox"><label for="shipping-address">Use this for my shipping address</label>
        <input id="billing-ddress" type="checkbox"><label for="billing-ddress">Use this for my billing address</label>
      </div>
    </div>
  </fieldset>
  <fieldset class="ussc-form__fieldset">
    <legend>Your Contact Information:</legend>
    <div class="row">
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Enter your email</span>
          <input type="email" placeholder="Enter your email">
        </label>
      </div>
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Confirm your email</span>
          <input type="email" placeholder="Confirm your email">
        </label>
      </div>
    </div>
    <div class="row">
      <div class="column medium-6 small-12">
        <label class="label--optional-text is-input-empty"><span class="ussc-form__input-label">Enter your telephone</span>
          <input type="tel" placeholder="Enter your telephone">
        </label>
      </div>
    </div>
  </fieldset>
  <fieldset class="ussc-form__fieldset">
    <legend>Create a Password:</legend>
    <div class="row">
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Enter your password</span>
          <input type="password" placeholder="Enter your password">
        </label>
      </div>
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Re-enter your password</span>
          <input type="password" placeholder="Re-enter your password">
        </label>
      </div>
    </div>
  </fieldset>
  <fieldset class="ussc-form__fieldset">
    <legend>Do You Own One of our Stoves?</legend>
    <div class="row">
      <div class="column small-12">
        <input id="own-stove--yes" type="checkbox"><label for="own-stove--yes">Yes, sure do</label>
        <input id="own-stove--no" type="checkbox"><label for="own-stove--no">No, not yet</label>
      </div>
    </div>
    <div class="row">
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Model Name</span>
          <input type="text" placeholder="Model Name">
        </label>
      </div>
      <div class="column medium-6 small-12">
        <label><span class="ussc-form__input-label">Serial Number</span>
          <input type="text" placeholder="Serial Number">
        </label>
      </div>
    </div>
  </fieldset>
  <fieldset class="ussc-form__fieldset">
    <legend>Radio buttons</legend>
    <div class="row">
      <div class="column small-12">
        <ol class="ussc-inputs-list">
          <li><input type="radio" name="radio-buttons" value="radio-1" id="radio-1" required><label for="radio-1">Radio Button 1</label></li>
          <li><input type="radio" name="radio-buttons" value="radio-2" id="radio-2"><label for="radio-2">Radio Button 2</label></li>
          <li><input type="radio" name="radio-buttons" value="radio-3" id="radio-3"><label for="radio-3">Radio Button 3</label></li>
        </ol>
      </div>
    </div>
  </fieldset>
  <fieldset>
    <div class="row">
      <div class="column small-12">
        <ol class="ussc-inputs-list">
          <li><input id="mail-signup" type="checkbox"><label for="mail-signup">I would like to receive email news.</label></li>
          <li><input id="privacy-policy" type="checkbox"><label for="privacy-policy">I've read and agree to the US Stove <a href="#">Privacy Policy</a></label></li>
        </ol>
      </div>
    </div>
  </fieldset>
  <div class="row">
    <div class="column small-12">
      <button class="ussc-form__button ussc-form__button--secondary">Create your account <i class="usstove-icon_interface_Arrow-Right"></i></button>
		</div>
	</div>
</form>

