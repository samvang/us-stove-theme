<?php
/**
 * Filename modal-search.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<div class="c-reveal reveal small" id="search" data-reveal>
	<button class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
	<div class="row">
		<div class="column small-12">
			<h2 class="reveal__title"><?php esc_html_e( 'Search US Stove', 'usstove' ); ?></h2>
		</div>
		<div class="column small-12">
			<?php get_product_search_form(); ?>
		</div>
	</div>
</div>
