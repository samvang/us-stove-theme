<?php
/**
 * Filename searchform.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input-group">
		<span class="screen-reader-text"><?php esc_html_e( 'Search for:', 'usstove' ); ?></span>
		<input class="input-group-field" type="search" placeholder="<?php esc_attr_e( 'Search &hellip;', 'usstove' ); ?>" value="<?php get_search_query(); ?>" name="s" />
		<div class="input-group-button">
			<input type="submit" class="button" value="<?php esc_attr_e( 'Search', 'usstove' ); ?>">
		</div>
	</div>
</form>
